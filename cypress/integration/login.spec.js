describe("/login", () => {
  beforeEach(() => {
    cy.fixture("server").as("data");
    cy.intercept("*server.json*", {
      fixture: "server.json",
    }).as("loginUser");
  });
  it("should show error message when error credentails is given", () => {
    cy.visit("/login");
    cy.get("input[name=email]").type("wrong@email.com");
    cy.get("input[name=password]").type("123WrongPassword");
    cy.get("button[type=submit]").click();
    cy.get(".credentials-error").should("be.visible");
  });

  it("should show error message when correct email is given with wrong password", function () {
    cy.visit("/login");
    cy.get("input[name=email]").type(this.data.users[0].email);
    cy.get("input[name=password]").type("123WrongPassword");
    cy.get("button[type=submit]").click();
    cy.wait("@loginUser");
    cy.get(".credentials-error").should("be.visible");
  });

  it("should login when correct credentials is given", function () {
    cy.visit("/login");
    cy.get("input[name=email]").type(this.data.users[0].email);
    cy.get("input[name=password]").type(this.data.users[0].password);
    cy.get("button[type=submit]").click();
    cy.wait("@loginUser");
    cy.url().should("include", "/welcome");
  });
  it("should show error message given invalid email", () => {
    cy.visit("/login");
    cy.get("input[name=email]")
      .type("dddd")
      .blur()
      .should("have.css", "border-color", "rgb(255, 0, 0)");
    cy.get(".error").should("be.visible");
  });
});
