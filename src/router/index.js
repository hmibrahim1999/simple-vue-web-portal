import { createRouter, createWebHistory } from "vue-router";
import LoginView from "../views/LoginView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/login",
      name: "login",
      component: LoginView,
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem("token")) {
          next("/welcome");
        } else {
          next();
        }
      },
    },
    {
      path: "/welcome",
      name: "welcome",
      component: () => import("../views/WelcomeView.vue"),
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem("token")) {
          next();
        } else {
          next("/login");
        }
      },
    },
    {
      path: "/404",
      name: "not-found",
      component: () => import("../views/NotFoundView.vue"),
    },
    {
      path: "/:catchAll(.*)",
      redirect: "/404",
    },
  ],
});

export default router;
