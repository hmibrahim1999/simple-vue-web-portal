export const baseUrl = "/server.json";

export default {
  getUser: async (id) => {
    const res = await fetch(baseUrl);
    const data = await res.json();
    return data.users[id];
  },
  loginUser: async (user) => {
    const res = await fetch(baseUrl);
    const data = await res.json();
    const index = data.users.findIndex(
      ({ email, password }) => email === user.email && password === user.password
    );
    return index;
  },
};
